import pygame
import sys

'''

Variables

'''

worldx_val = 1506
worldy_val = 600
fps = 144
ani = 4
world = pygame.display.set_mode([worldx_val, worldy_val])
player_dead = False

#Color Declarations
ALPHA = (0, 255, 0)
BLACK = (0, 0, 0)
RED = (255, 0 ,0)

'''

Objects

'''

class Camera:
    def _init_(self, Player, vec):
        self.Player = Player
        self.offset = vec(0,0)
        self.offset = vec(0,0)

        self.CamH = 200
        self.CamW = 100

        self.CONST = vec(-self.CamW / 2 + Player.rect.x / 2, -self.Player.rect.y + 20)

    def setMethod(self, method):
        self.method = method

    def scroll(self):
        self.method.scroll()



class CamScroll():
    def _init_(self,camera,Player):
        self.camera = camera
        self.Player = Player


class Follow(CamScroll):
    def _init_(self,camera,Player):
        CamScroll._init_(self,camera,Player)


    def scroll(self):
        self.camera.offset_float.x += (self.player.rect.x - self.camera.offset_float.x + self.camera.CONST.x)
        self.camera.offset_float.y += (self.player.rect.y - self.camera.offset_float.y + self.camera.CONST.y)
        self.camera.offset.x, self.camera.offset.y = int(self.camera.offset_float.x), int(self.camera.offset_float.y)

class Border(CamScroll):
    def _init_(self,camera,player):
        CamScroll._init_(self,camera,player)


        def scroll(self):
            self.camera.offset_float.x += (self.player.rect.x - self.camera.offset_float.x + self.camera.CONST.x)
            self.camera.offset_float.y += (self.player.rect.y - self.camera.offset_float.y + self.camera.CONST.y)
            self.camera.offset.x, self.camera.offset.y = int(self.camera.offset_float.x), int(self.camera.offset_float.y)
            self.camera.offset.x = max(self.player.left_border, self.camera.offset.x)
            self.camera.offset.x = min(self.camera.offset.x, self.player.right_border - self.camera.CamW)


class Auto(CamScroll):
    def _init_(self,camera,player):
        CamScroll._init_(self,camera,player)

class Enemy(object):
    enemy_right = [pygame.image.load('C:\images\EnemyR1.png')]
    enemy_left = [pygame.image.load('C:\images\EnemyL1.png')]
    enemy_die = []

    #spawn enemy
    def __init__(self,x,y,width,height,end):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 5
        self.path = [x,end]
        self.walkCount = 0
        self.hitbox = (self.x, self.y+2, 62,57)

    def draw(self, win):
        self.move()
        if self.walkCount + 1 >= 33: 
            self.walkCount = 0
        if self.vel > 0:
            win.blit(self.enemy_right[0], (self.x,self.y))
            self.walkCount += 1
        else:
            win.blit(self.enemy_left[0], (self.x,self.y))
            self.walkCount += 1
        self.hitbox = (self.x, self.y+2, 62, 57)
        pygame.draw.rect(win, (255,0,0), self.hitbox,2)

    def move(self):
        if self.vel > 0:
            if self.x < self.path[1] + self.vel:
                self.x += self.vel
            else:
                self.vel = self.vel * -1
                self.x += self.vel
                self.walkCount = 0
        else:
            if self.x > self.path[0] - self.vel:
                self.x += self.vel
            else:
                self.vel = self.vel * -1
                self.x += self.vel
                self.walkCount = 0

    def hit(self):
        print('hit')


class Platform(pygame.sprite.Sprite):
    def __init__(self, xloc, yloc, imgw, imgh, img):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('C:\images\plat.png').convert()
        self.image.convert_alpha()
        self.image.set_colorkey(ALPHA)
        self.rect = self.image.get_rect()
        self.rect.y = yloc
        self.rect.x = xloc


class Player(pygame.sprite.Sprite):
    """
    
    Spawn a player

    """

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.movex = 0
        self.movey = 0
        self.frame = 0
        self.health = 3
        self.is_jumping = True
        self.is_falling = True
        self.images = []
        for i in range(1, 5):
            img = pygame.image.load('C:\images\main_char.png').convert()
            img.convert_alpha()
            img.set_colorkey(ALPHA)
            self.images.append(img)
            self.image = self.images[0]
            self.rect = self.image.get_rect()

    def gravity(self):
        if self.is_jumping:
            self.movey += 1.2

    def control(self, x, y):
        """
        control player movement

        """
        self.movex += x

    def jump(self):
        if self.is_jumping is False:
            self.is_falling = False
            self.is_jumping = True

    def update(self):

        if self.movex < 0:
            self.is_jumping = True
            self.frame += 1
            if self.frame > 3 * ani:
                self.frame = 0
            self.image = pygame.transform.flip(self.images[self.frame // ani], True, False)

        if self.movex > 0:
            self.is_jumping = True
            self.frame += 1
            if self.frame > 3 * ani:
                self.frame = 0
            self.image = self.images[self.frame // ani]

        ground_hit_list = pygame.sprite.spritecollide(self, ground_list, False)
        for g in ground_hit_list:
            self.movey = 0
            self.rect.bottom = g.rect.top
            self.is_jumping = False

        if self.rect.y > worldy_val:
            self.health -=1
            self.rect.x = tx
            self.rect.y = ty
        if self.rect.x > worldx_val:
            self.health -=1
            self.rect.x = tx
            self.rect.y = ty
        
        if self.health <= 0:
            global player_dead
            player_dead = True

        plat_hit_list = pygame.sprite.spritecollide(self, plat_list, False)
        for p in plat_hit_list:

            self.is_jumping = False  # stop jumping
            self.movey = 0
            if self.rect.bottom <= p.rect.bottom:
               self.rect.bottom = p.rect.top
            else:
               self.movey += 3.2

        if self.is_jumping and self.is_falling is False:
            self.is_falling = True
            self.movey -= 22

        self.rect.x += self.movex
        self.rect.y += self.movey


    def platform(lvl, tx, ty):
        plat_list = pygame.sprite.Group()
        ploc = []
        i = 0
        if lvl == 1:
            ploc.append((200, worldy_val - ty - 128, 3))
            ploc.append((300, worldy_val - ty - 256, 3))
            ploc.append((550, worldy_val - ty - 128, 4))
            while i < len(ploc):
                j = 0
                while j <= ploc[i][2]:
                    plat = Platform((ploc[i][0] + (j * tx)), ploc[i][1], tx, ty, 'plat.png')
                    plat_list.add(plat)
                    j = j + 1
                print('run' + str(i) + str(ploc[i]))
                i = i + 1

        if lvl == 2:
            print("Level " + str(lvl))

        return plat_list

class Level:
    def ground(lvl, gloc, tx, ty):
        ground_list = pygame.sprite.Group()
        i = 0
        if lvl == 1:
            while i < len(gloc):
                ground = Platform(gloc[i], worldy_val -ty, tx, ty, 'plat.png')
                ground_list.add(ground)
                i = i + 1
        if lvl == 2:
            print("Level " +str(lvl))
        return ground_list
    
    def platform(lvl, tx, ty):
        plat_list = pygame.sprite.Group()
        ploc = []
        i = 0
        if lvl == 1:
            ploc.append((200, worldy_val - ty - 64, 2))
            ploc.append((300, worldy_val - ty - 128, 3))
            ploc.append((500, worldy_val - ty - 64, 4))
            while i < len(ploc):
                j = 0
                while j <= ploc[i][2]:
                    plat = Platform((ploc[i][0] + (j * tx)), ploc [i][1], tx, ty, 'tile.png')
                    plat_list.add(plat)
                    j = j + 1
                print('run' + str(i) + str(ploc[i]))
                i = i + 1
            
            if lvl == 2:
                print("Level " + str(lvl))
            
            return plat_list
        

'''
Setup
'''

backdrop = pygame.image.load('C:\images\landscape.png')
clock = pygame.time.Clock()
pygame.init()
backdropbox = world.get_rect()
main = True

player = Player()
enemy = Enemy(100,100,64,64,300)
player.rect.x = 0
player.rect.y = 30
player_list = pygame.sprite.Group()
player_list.add(player)
steps = 10

gloc = []
tx = 64
ty = 64

i = 0
while i <= (worldx_val / tx) + tx:
    gloc.append(i * tx)
    i = i + 1

ground_list = Level.ground(1, gloc, tx, ty)
plat_list = Level.platform(1, tx, ty)

'''
Main Loop
'''

while main:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            try:
                sys.exit()
            finally:
                main = False

        if event.type == pygame.KEYDOWN:
            if event.key == ord('q'):
                pygame.quit()
                try:
                    sys.exit()
                finally:
                    main = False
            if event.key == pygame.K_LEFT or event.key == ord('a'):
                player.control(-steps, 0)
            if event.key == pygame.K_RIGHT or event.key == ord('d'):
                player.control(steps, 0)
            if event.key == pygame.K_SPACE or event.key == ord('w'):
                player.jump()

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == ord('a'):
                player.control(steps, 0)
            if event.key == pygame.K_RIGHT or event.key == ord('d'):
                player.control(-steps, 0)

    world.blit(backdrop, backdropbox)
    player.update()
    player.gravity()
    enemy.move()
    enemy.draw(world)
    player_list.draw(world)
    ground_list.draw(world)
    plat_list.draw(world)
    if player_dead:
        font = pygame.font.Font('freesansbold.ttf', 32)
        text = font.render('You Died! Press Q to exit the game!', True, RED, BLACK)
        textRect = text.get_rect()
        textRect.center = (worldx_val //2, worldy_val //2)
        world.fill(BLACK)
        world.blit(text, textRect)
    pygame.display.flip()
    clock.tick(fps)